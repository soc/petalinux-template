# Linux patches

**Purpose**: currently no functional purpose - add your own configurations!

It currently contains an example file removing a few features that slow down booting or compilation and are not relevant to a SoC. It is a starting point to showcase how to set kernel compile-time options.