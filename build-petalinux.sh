#!/bin/bash

set -e

if [[ "$BOARD_NAME" == "" ]]; then
    echo "You must set a BOARD_NAME"
    exit 1
fi

source ${PETALINUX_HOME:-/opt/Xilinx/petalinux}/settings.sh

PL_TEMPLATE=zynqMP

petalinux-create -t project --template $PL_TEMPLATE -n l1ct-peta
cd l1ct-peta

# This is required because (at the time of writing) the base Docker image has GCC<6
# When we will move to Alma, check if this is still needed (G. Muscarello, 2023-02-23)
echo 'CONFIG_YOCTO_BUILDTOOLS_EXTENDED=y' >> project-spec/configs/config

# Run petalinux-config once to set defaults, then patch them, then re-run petalinux-config
# The second run is required to set some variables from the general config file
petalinux-config --silentconfig --get-hw-description ../boards/$BOARD_NAME/xsa
if [[ $? -ne 0 ]]; then
        cat build/config.log
        exit 1
fi
../apply-config.sh $PWD
petalinux-config --silentconfig

# Build, and print the error log if the build fails
set +e
petalinux-build 2>&1 | tee /tmp/local-build.log
EXIT_CODE=$?
if [[ EXIT_CODE -ne 0 ]]; then
    FAILURE_LOGFILE=$(grep 'ERROR: Logfile of failure stored in: ' /tmp/local-build.log | cut -d' ' -f7)
    echo "[CI] Content of $FAILURE_LOGFILE:"
    cat ${FAILURE_LOGFILE:-/dev/null} # Prevents cat from hanging if no failure logfile is detected
    echo '[CI] Content of build/build.log:'
    cat build/build.log
    exit $EXIT_CODE
fi
set -e

# The file is called psu_init.tcl on aarch64 and ps7_init.tcl on armv7hl
cp project-spec/hw-description/ps*_init.tcl images/linux/
petalinux-package --boot --u-boot --fpga ../boards/$BOARD_NAME/bitfile

# rm -rf build components
