# petalinux-template

This project is a template for building Petalinux projects. Out of the box, it comes with:

 - scripts for interactive or automated building of boot files
 - a workflow that supports multiple boards with different hardware
 - reading geographic information from the IPMC using SIPL
 - location-aware booting with DHCP Client ID

It is meant to provide a common and flexible Petalinux infrastructure that you can fork adding your own patches, configurations and layers. In this sense, it doesn't lock you into any specific hardware, booting method or design choice.

It is currently in use in the [ATLAS Level 1 Central Trigger group](https://gitlab.cern.ch/atlas-l1ct/) to build boot files for the MUCTPI and LTI SoCs.

## Overview

This project is a starting template for Petalinux that comes with a few base layers built in. It is meant to be forked to add project-specific patches and layers, and then optionally pulled in from time to time to profit from new features.

The `layers` directory contains patches (Yocto layers) that apply to all hardware platforms. They are strictly ordered by a number. We currently provide two layers:

 1. `0-soc-ig-common`, adding generic DHCP Client ID support in U-Boot and providing a smaller kernel
 2. `1-u-boot-sipl`, adding SIPL support in U-Boot to read a shelf identity from an IPMC and generate a DHCP Client ID from it

Users are meant to add more layers after these (eg. to add new features, or to specify a custom algorithm to generate the client ID).

The `boards` directory contains hardware-specific patches. It contains one subdirectory per board type (for example, `boards/muctpi` and `boards/lti`). These subdirectories are themselves Yocto layers and will be applied as the latest layer. We provide an example directory that merely adds a few device tree patches and enables SIPL support; to use it, replace the empty files `xsa` and `bitfile` with the proper files from your hardware design, using these exact filenames.

When the project is built, the script will do the following:

 1. Create a project from the user-provided XSA
 2. Build the device tree with board-specific patches
 3. Build U-Boot with Client ID/SIPL patches, plus any other patches you may add
 4. Build the Linux kernel with a few safe options like `CONFIG_SOUND=n`, plus any other patches you may add
 5. Build the rootfs
 5. Package the build as a BOOT.BIN

The project supports both an interactive workflow in the shell, where it will produce a Petalinux project directory that you can work on, and an automated workflow, where you integrate the build process in your CI. We intentionally do not maintain a specific CI workflow, but we provide an example implementation.

## Building

After cloning this repository with `--recursive`, the bare minimum you need to build a project is to:

 1. Choose a board name, eg. `BOARD_NAME=muctpi`
 2. Create `boards/$BOARD_NAME`
 3. Create `boards/$BOARD_NAME/conf/layer.conf`; you can just copy `boards/example/conf/layer.conf`
 4. Symlink `boards/$BOARD_NAME/xsa` to your .xsa file, and `boards/$BOARD_NAME/bitfile` to your bitfile
 5. Create `boards/$BOARD_NAME/recipes-bsp/device-tree/files/system-user.dtsi` with your device tree patches
 6. Adapt `build-petalinux.sh` to your Petalinux installation path and board type (Zynq/ZynqMP)

And that's it! You can then run `./build-petalinux.sh` to automatically create a project, apply the patches and build.

Once built you can then make changes to the newly created Petalinux project, just like you usually do. Changes made to the project are temporary; you will probably want to commit "permanent" changes into `boards`.

## Customizing

You can add your own layers into the project by simply creating a directory like `layers/NN-layer-name`. Refer to the [Yocto documentation](https://docs.yoctoproject.org/dev/dev-manual/layers.html) for details on how to create layers.

> If you already have a Petalinux project, note that `project-spec/meta-user` is itself a layer; you can copy it into `layers` as a starting point.

In an existing project, you can use `petalinux-devtool` to check out a component and make changes to its source code:

``` bash
# For the kernel
petalinux-devtool modify linux-xlnx
# For U-Boot
petalinux-devtool modify u-boot-xlnx
# For the FSBL
petalinux-devtool modify fsbl-firmware
```

This will check out the selected package under `components/yocto/workspace/sources`. You can make modifications there and they will be included in the build (Note that for fsbl you might need to run `petalinux-build -c fsbl -x distclean` first, otherwise your changes might not be included)

If you want to generate patches from your changes, you should commit your files and run:

```sh
petalinux-devtool finish $COMPONENT $PWD/modification-layer -f
```

The directory `modification-layer` will contain your changes together with the base layers. Specifically, the commits you made will be represented as .patch files that you can then move to the "proper" layer (under `layers/`). To enable these patches, add them to SRC_URI in the .bbappend file:

```
SRC_URI:append = " file://0001-Add-foo-bar.patch"
```

To reset your changes you can run:
```sh
petalinux-devtool reset $COMPONENT
```

## Continuous Integration

We intentionally do not provide a specific CI workflow so that you are free to bring your own. However, if you are getting started we propose here an example workflow for continuous integration:

```yaml
# .gitlab-ci.yml

image: your-petalinux-image-here:$PETALINUX_VERSION
variables:
  PETALINUX_VERSION: "2022.2"
  BOARD_NAME: muctpi
script:
  - /home/petalinux/build-petalinux.sh
  - tar czvf images.tar.gz l1ct-peta/images/linux/*
artifacts:
  - images.tar.gz
```

Some things you may want to pay attention to:
 - use a dedicated runner instead of a shared one, so Petalinux images (which are ~20 GB in size) can be cached
 - configure a sstate and downloads cache to speed up builds

## Miscellaneous

### Caching downloads and builds

This is optional but strongly recommended to speed up successive builds.

```sh
vim components/yocto/layers/meta-petalinux/conf/distro/petalinux.conf
# Add these lines with a directory of your choice:
# DL_DIR="/home/gmuscare/pl-downloads-2022.1"
# SSTATE_DIR="/home/gmuscare/pl-sstate-2022.1"
```

### Keeping intermediate artifacts

By default Petalinux removes all intermediate artifacts. If you want to keep them for a given recipe, add the following line to the `project-spec/meta-user/conf/petalinuxbsp.conf` file:

```bash
RM_WORK_EXCLUDE += "recipe"
```

if you want to keep all intermediate artifacts, add the following line ot the `project-spec/meta-user/conf/petalinuxbsp.conf` file:

```bash
INHERIT:remove = "rm_work"
```

### Configuring Client ID in DHCP servers

**dhcpd**:

```
# /etc/dhcp/dhcpd.conf

subnet 192.168.1.0 netmask 255.255.255.0 {
  # unknown MACs will be temporarily assigned an IP from this range
  range 192.168.1.100 192.168.1.254;

  # the board with MAC 00:0a:35:00:22:01 will be assigned the IP 192.168.1.10 and the hostname lti-1
  host lti1 {
    hardware ethernet 00:0a:35:00:22:01;
    fixed-address 192.168.1.10;
    option host-name lti-1;
  }
}
```

Matching by client ID is tried first, then matching by MAC address.

If in doubt you can capture packets with `tcpdump -vvvnne 'udp port 68'`: `-vvv` shows lots of details, `-nn` disables reverse DNS, and `-e` shows MAC addresses.

## Credits

 - Giulio Muscarello: multi-board support
 - Ralf Spiwoks: SIPL implementation in U-Boot
 - Petr Žejdl: DHCP Client ID support
