#!/bin/bash
set -e

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

if [ "$#" -ne 1 ]; then
    echo "Illegal number of parameters, provide the path to your petalinux work directory"
    exit 1
fi
if [[ "$BOARD_NAME" == "" ]]; then
    echo "You must set a BOARD_NAME"
    exit 1
fi

export PETALINUX_DIR=$(realpath $1)

function set_in_kconfig() {
  local key="$1"
  local value="$2"
  local CONFIG_FILE=$PETALINUX_DIR/project-spec/configs/config

  if grep -q "^$key=" $CONFIG_FILE; then
    sed -i -e "s|^\(# \)\?$key\b.*|$key=$value|g" $CONFIG_FILE
  else
    echo "$key=$value" >> $CONFIG_FILE
  fi
}


# Adds debug statements during boot
echo "Patching ATF debug flag in config..."
set_in_kconfig CONFIG_SUBSYSTEM_ATF_DEBUG y
echo "Patching FSBL debug flag in config..."
set_in_kconfig CONFIG_SUBSYSTEM_FSBL_COMPILER_EXTRA_FLAGS '"-DFSBL_DEBUG_DETAILED=1"'

# This flag is required for NFS to work, otherwise it will fail silently
echo "Patching NFS flag in config..."
set_in_kconfig CONFIG_SUBSYSTEM_ROOTFS_NFS y

# Uncomment these lines if needed for your project
# echo "Disabling rootfs packaging..."
# set_in_kconfig CONFIG_SUBSYSTEM_RFS_FORMATS '""'

# Add a local sstate cache here if desired
# echo "Enabling local sstate cache..."
# SSTATE_URL="http://pcphese91.dyndns.cern.ch:8080/sstate-${PETALINUX_VERSION}/aarch64/"
# if curl --output /dev/null --silent --head --fail "$SSTATE_URL"; then
#   set_in_kconfig CONFIG_YOCTO_NETWORK_SSTATE_FEEDS_URL "\"$SSTATE_URL\""
#   # Simply changing the config file isn't enough, we must also change plnxtool.conf to append the string "file://.* $SSTATE_URL \n \"
#   sed -i '/^SSTATE_MIRRORS =/a file://.* $SSTATE_URL \\n \\' $PETALINUX_DIR/build/conf/plnxtool.conf
# else
#   echo "Unable to reach sstate cache URL: $SSTATE_URL"
#   curl -vI "$SSTATE_URL"
#   exit 1
# fi

echo "Applying recipes..."
# How do we apply recipes in a consistent and correct order?
# First, we require users to adopt a consistent scheme for numbering layers
# Secondly, we glob with the "C" locale so the ordering is correct and not broken by locale
export LC_ALL=C
i=0
for layer in $SCRIPT_DIR/layers/*; do
  LAYER_NAME="$(basename $layer)"
  if [[ ! "$LAYER_NAME" =~ ^[0-9]+ ]]; then
    echo "Layer $LAYER_NAME should be renamed: layers should begin with a number to ensure consistent ordering"
    exit 1
  fi
  echo "#$i: Applying layer $LAYER_NAME"
  set_in_kconfig "CONFIG_USER_LAYER_$i" "\"$(realpath $layer)\""
  i=$((i + 1))
done

echo "#$i: Applying layer $BOARD_NAME"
set_in_kconfig "CONFIG_USER_LAYER_$i" "\"$(realpath $SCRIPT_DIR/boards/$BOARD_NAME)\""
i=$((i + 1))
echo "Copying system-user.dtsi to meta-user"
cp $SCRIPT_DIR/boards/$BOARD_NAME/recipes-bsp/device-tree/files/system-user.dtsi $PETALINUX_DIR/project-spec/meta-user/recipes-bsp/device-tree/files/system-user.dtsi

echo 'Done! Now, re-run petalinux-config.'
